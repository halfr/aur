#!/usr/bin/env bash
curl https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=prometheus-node-exporter-git > prometheus-node-exporter/PKGBUILD
curl https://aur.archlinux.org/cgit/aur.git/plain/prometheus-node-exporter.service?h=prometheus-node-exporter-git > prometheus-node-exporter/prometheus-node-exporter.service

# vim:set tw=0:
